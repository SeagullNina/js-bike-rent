'use strict';

function cardNumberValidate(cardNumberValue){
    hideError(cardNumberValue.target);
    let value = cardNumberValue.target.value;
    if(cardNumberValue.type === 'blur' && !value) 
    {
        return;
    }
    if (!/^\d{4}([ ]?)(\d{4}\1?\d{4}\1?\d{4})$/.test(value))
    {
    renderError("Введите правильный номер карты", cardNumberValue.target);
    return;
    }
    value = value.replace(/(\d)(?=(\d\d\d\d)+([^\d]|$))/g, '$1 ');
    cardNumberValue.target.value = value;
}

document.getElementById("number").addEventListener("blur", cardNumberValidate);

function cardDateValidation(cardDateValue){
    hideError(cardDateValue.target);
    let value = cardDateValue.target.value;
    if(cardDateValue.type === 'blur' && !value)
    {
        return;
    }
    if(value.length === 2 && !value.split('/')[1] || (value.length > 3 && value.length < 4) || (!!+value && value.length === 4)) 
    {
        cardDateValue.target.value = `${value.substr(0,2)}/${value.substr(2)}`
    }
    let year = value.split('/')[1];
    const now = new Date().getFullYear().toString().slice(2);
    if(!/^(0[1-9]|1[0-2])\/?([0-9]{2})/.test(value) || year < now || year > +now + 5) 
    {
        renderError("Введите правильную дату", cardDateValue.target);
        return;
    }
}

document.getElementById("term").addEventListener("blur", cardDateValidation);

function cvvValidate(cardCvvValue){
    hideError(cardCvvValue.target);
    let value = cardCvvValue.target.value;
    if(cardCvvValue.type === 'blur' && !value) return;
            if (!/^[0-9]{3}/.test(value) || value.length !== 3) {
                renderError("Введите правильный CVV", cardCvvValue.target);
                return;
            }
}
document.getElementById("cvv").addEventListener("blur", cvvValidate);

function renderError(error, elem){
    elem.classList.add('errorInput');
    const tooltip = document.createElement('span');
    tooltip.className = 'tooltip';
    tooltip.textContent = error;
    elem.after(tooltip);
}

function hideError(elem){
    elem.classList.remove('errorInput');
    let tooltipList = elem.parentElement.getElementsByClassName('tooltip');
    if (tooltipList.length){
      for (let tooltip of tooltipList){
        tooltip.remove();
      }
    }
}

function saveRequisites(date, number, cvv) {
    let options = {
      method: "PUT",
      credentials: "include",
      headers: {
          "Content-Type": "application/json"
      },
      body: JSON.stringify({date, number, cvv})
  };
  return fetch('/api/card-requisites', options)
      .then(response => {
          if (response.status === 201) 
          {
              return Promise.resolve(response);
          } 
          else 
          {
              return Promise.reject(response.status + "  " + response.statusText);
          }
      })
      .then(response => response.json())
      .then(object => Promise.resolve(object));
  }

function validateForm(){
document.getElementById("form").addEventListener("submit", cardNumberValidate);
document.getElementById("form").addEventListener("submit", cvvValidate);
document.getElementById("form").addEventListener("submit", cardDateValidation);
}
const cardRequisitesForm = document.getElementById('form');

cardRequisitesForm.addEventListener('submit', function (event) {
    validateForm();
    saveRequisites(document.getElementById("term").value, document.getElementById("number").value, document.getElementById("cvv").value);
    window.history.back();
  event.preventDefault();
});
