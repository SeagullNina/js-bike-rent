'use strict';
function init() {
  let page = 1;
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  loadCatalog(page);
  const loadMoreBtn = document.getElementById("loadMore");
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  loadMoreBtn.onclick = () => {
    disableButtonLoadMore();
    loadCatalog(page++).then(() => {
      enableButtonLoadMore();
    });
  }
}
async function getBikes(pointId, page) {
  return await fetch(`/api/catalog/${pointId}?page=${page}`)
    .then(response => response.status === 200 ? Promise.resolve(response) : Promise.reject(response))
    .then(response => response.json())
}


function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore

  getBikes(getPointId(), page).then(results => {
    appendCatalog(results.bikesList);
    showButtonLoadMore(results.hasMore);
  });
}
/**
 * Создает HTML тэг с необязательными параметрами content и properties
 *
 * {string} tag - HTML тэг, например 'a', 'div', 'p'
 * {string} content - текстовое содержимое тэга (необязательный параметр)
 * {Array[{name: string, value: string}]} properties - массив объектов со свойствами HTML тэга
 */

function createElem(tag, content = false, properties = false) {
  let element = document.createElement(tag);
  if(content) element.appendChild(document.createTextNode(content));
  if(properties) properties.map(property => element.setAttribute(property.name, property.value));
  return element;
}

function appendCatalog(items) {
 // отрисуй велосипеды из items в блоке <div id="bikeList">
 const list = document.getElementById("bikeList");
 items.map(item => {
   let card = createElem('div', false, [{ name: 'class', value: 'card' }]);
   let imgProps = [
   {
     name: 'width',
     value: '320px',
   },
   {
     name: 'src',
     value: `../images/${item.img}`
   }];
   let btnProps = [{ name: 'type', value: 'button' }, { name: 'value', value: 'Арендовать' }, { name: 'class', value: 'rentBtn' }]
   card.appendChild(createElem('img', false, imgProps));
   card.appendChild(createElem('h4', item.name));
   card.appendChild(createElem('h4', `Стоимость за час - ${item.cost * 60} ₽`));
   card.appendChild(createElem('input', false, btnProps));
   list.appendChild(card);
 })
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  if (hasMore == true)
  document.getElementById("loadMore").hidden = false;
  else
  document.getElementById("loadMore").hidden = true;
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  document.getElementsByClassName("loadBtn").disabled = true;
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  document.getElementsByClassName("loadBtn").disabled = false;
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  const currentId = document.URL.split("/");
  return currentId[currentId.length - 1] !== "catalog" ? currentId[currentId.length - 1] : "";
}


document.addEventListener('DOMContentLoaded', init)
