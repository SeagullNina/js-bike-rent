'use strict';

function returnBike(orderId, target, parentElement)
{
    let options = {
        method: "DELETE",
        credentials: "include",
        headers: {
            "Content-Type": "application/json"
        }
    };
    return fetch(`/api/order/${orderId}`, options)
        .then(response => {
            if (response.status === 200) 
            {
                parentElement.removeChild(target);
                return Promise.resolve(response);
            } 
            else 
            {
                return Promise.reject(response.status + "  " + response.statusText);
            }
        })
        .then(response => response.json())
        .then(object => Promise.resolve(object));
}

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('bikesList').addEventListener('click', (event) => {
      if (event.target.classList.contains('replace')) {
        returnBike(event.target.dataset.orderId, event.currentTarget, event.target.parentElement);
      }
    })
  })
  